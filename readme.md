# dotfiles
My settings for i3-gaps <br />
using i3lock, urxvt, ranger and nitrogen <br />
using xcwd for new terminal with same directory

### i3 check configuration
`i3 --more-version`

### URxvt setting .Xresource file
`urxvt --help 2>&1| sed -n '/:  /s/^ */! URxvt*/gp' >> ~/.Xresources`

### Install ranger
```
cd ~/temp
git clone https://github.com/hut/ranger.git
cd ranger
sudo make install
```

## Reference
```
https://www.maketecheasier.com/install-use-i3-window-manager-ubuntu/
https://faq.i3wm.org/question/3747/enabling-multimedia-keys.1.html
https://askubuntu.com/questions/823724/status-bar-icons-i3-window-manager
https://faq.i3wm.org/question/239/how-do-i-suspendlockscreen-and-logout.1.html
https://github.com/schischi/xcwd
https://codeyarns.com/2013/08/26/how-to-install-and-use-ranger/
```
## Things To Do

1. Low battery warning
2. Brightness control
3. Background Wallpaper